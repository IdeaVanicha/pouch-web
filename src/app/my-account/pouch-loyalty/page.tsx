"use client";
import { MyAccountContainer } from "../MyAccountContainer";
import { MyAccountPage } from "../MyAccountPageTracker";
import { useState } from "react";
import { AddItemModal } from "../../../design-system/Modal/AddItemModal";
import { DeleteItemModal } from "../../../design-system/Modal/DeleteItemModal";
import { useMyAccount } from "../MyAccountProvider";
import {
  PouchLoyaltyTable,
  PouchLoyaltyTableType,
} from "@/app/my-account/pouch-loyalty/PouchLoyaltyTable";
import { PouchLoyalty } from "../../../../temp/PartnerApiType";

const mapLoyaltyProgramToTableItems = (
  rewardProgram: PouchLoyalty[],
): PouchLoyaltyTableType[] => {
  return rewardProgram.map((program) => {
    return {
      name: program.partnerDefined.name,
      pointsRequired: program.partnerDefined.pointsRequired,
      image: <></>,
      active: false,
    } as PouchLoyaltyTableType;
  });
};

export default function PouchLoyaltyPage() {
  const [addItemVisible, setAddItemVisible] = useState(false);
  const [deleteItemModal, setDeleteItemModal] = useState<{
    visible: boolean;
    id: string | undefined;
  }>({ visible: false, id: undefined });
  const { partnerData, updatePouchLoyaltyProgram } = useMyAccount();
  const { rewardProgram } = partnerData;

  return (
    <>
      <MyAccountContainer currentPage={MyAccountPage.PouchLoyalty}>
        <div className="w-full mt-16 flex justify-center items-center">
          <div className="w-[1200px]">
            {rewardProgram && (
              <PouchLoyaltyTable
                data={mapLoyaltyProgramToTableItems(rewardProgram)}
                addItem={() => setAddItemVisible(true)}
                // deleteItem={() =>   setDeleteItemModal({ visible: true, id: value._id })}
              />
            )}
          </div>
        </div>
        {/* <div className="w-fill mt-16 flex justify-between items-center">
          <Title value="Active Loyalty Program" />
          <CircularButton
            size="md"
            onClick={() => setAddItemVisible(true)}
            icon={<FaPlus color="#ffffff" />}
          />
        </div>
        <div className="w-full pb-4 flex space-x-8 overflow-x-auto">
          {rewardProgram &&
            rewardProgram.map((value, key) => (
              <ItemCard
                key={key}
                deleteItem={() =>
                  setDeleteItemModal({ visible: true, id: value._id })
                }
                item={value}
              />
            ))}
        </div> */}
      </MyAccountContainer>
      <AddItemModal
        close={() => setAddItemVisible(false)}
        isVisible={addItemVisible}
        updatePouchLoyaltyProgram={updatePouchLoyaltyProgram}
      />
      <DeleteItemModal
        close={() => setDeleteItemModal({ visible: false, id: undefined })}
        isVisible={deleteItemModal.visible}
        updatePouchLoyaltyProgram={updatePouchLoyaltyProgram}
        programId={deleteItemModal.id || ""}
      />
    </>
  );
}
