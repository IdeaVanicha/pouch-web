import { FaTrashCan } from "react-icons/fa6";
import { Table } from "@/design-system/Table/Table";
import {
  TableColumn,
  TableHeaderColumn,
  TableRow,
} from "@/design-system/Table/TableElement";

export type PouchLoyaltyTableType = {
  name: string;
  pointsRequired: number;
  image: React.ReactElement;
  active: boolean;
};

type TableItemsType = {
  data: PouchLoyaltyTableType[];
  addItem: () => void;
};

const mapTableKeyToDisplayName = (key: string) => {
  switch (key) {
    case "name":
      return "Name";
    case "pointsRequired":
      return "Points Required";
    case "image":
      return "Image";
    case "active":
      return "Status";
    default:
      return undefined;
  }
};

const PouchLoyaltyTitles: (keyof PouchLoyaltyTableType)[] = [
  "name",
  "active",
  "pointsRequired",
  "image",
];

export function PouchLoyaltyTable({ data, addItem }: TableItemsType) {
  return (
    <Table
      title="Rewards"
      addItemButton={addItem}
      itemsCount={data.length}
      headersElements={
        <>
          {PouchLoyaltyTitles.map((Title, key) => (
            <TableHeaderColumn
              key={key}
              className={key !== 0 ? "justify-center" : " justify-between"}
            >
              <p>{mapTableKeyToDisplayName(Title)}</p>
            </TableHeaderColumn>
          ))}
          <TableHeaderColumn className="justify-center">
            <p>Remove</p>
          </TableHeaderColumn>
        </>
      }
      dataElements={
        <>
          {data.map((value, key) => (
            <TableRow key={key}>
              {PouchLoyaltyTitles.map((Title, key) => {
                return (
                  <TableColumn
                    key={key}
                    className={
                      key !== 0 ? "justify-center" : " justify-between"
                    }
                  >
                    {value[Title]}
                  </TableColumn>
                );
              })}
              <TableColumn className="justify-center">
                <button>
                  <FaTrashCan color="#FFC04E" size={25} />
                </button>
              </TableColumn>
            </TableRow>
          ))}
        </>
      }
    />
  );
}
