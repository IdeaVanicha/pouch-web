"use client";
import PillButton from "@/design-system/Button/PillButton";
import PillInputBar from "@/design-system/Input/PillInputBar";
import { Title } from "@/design-system/Text/Text";
import Link from "next/link";
import { MdEmail, MdLock } from "react-icons/md";
import logo from "./Screenshot 2024-03-18 at 11.22 1.png";
import Image from "next/image";
import { auth } from "@/firebase/firebase";
import { UserCredential, signInWithEmailAndPassword } from "firebase/auth";
import { useEffect, useState } from "react";
import { useMyAccount } from "../MyAccountProvider";
import { HttpsCallable } from "firebase/functions";
import { useRouter } from "next/navigation";

export default function Login() {
  const router = useRouter();
  const { setPartnerId } = useMyAccount();
  const [details, setDetails] = useState<{
    email: string | undefined;
    password: string | undefined;
  }>({ email: undefined, password: undefined });

  const login = async () => {
    if (details.email && details.password) {
      await signInWithEmailAndPassword(auth, details.email, "Password@temp6589")
        .then((data: UserCredential) => {
          setPartnerId(data.user.uid);
          localStorage.setItem("partner-id", JSON.stringify(data.user.uid));
          router.push(`/my-account/dashboard`);
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <div className="h-full flex flex-col items-center justify-center">
      <div className="top-0 w-full absolute p-8">
        <Link href={"https://pouch-app.webflow.io/"} target="_blank">
          <Image src={logo} width={120} alt="Logo" />
        </Link>
      </div>
      <div className="w-96 h-96 flex flex-col items-center space-y-2">
        <Title value="👋 Welcome to Pouch!" classname="mb-6" />
        <PillInputBar
          onChange={(value) => setDetails(() => ({ ...details, email: value }))}
          icon={<MdEmail color="#DDDDDD" />}
          placeholder="Email"
        />
        <PillInputBar
          onChange={(value) =>
            setDetails(() => ({ ...details, password: value }))
          }
          icon={<MdLock color="#DDDDDD" />}
          placeholder="Password"
        />
        <div className="w-full flex justify-end">
          <a className="text-left">Forgot Password?</a>
        </div>
        <PillButton
          onClick={() => login()}
          text="Submit"
          bgColor="#552084"
          variant="primary"
        />
        <div className="flex mt-6">
          <p>Don&apos;t have an account? </p>
          <Link href={`/my-account/sign-up`}>Join Us!</Link>
        </div>
      </div>
    </div>
  );
}
