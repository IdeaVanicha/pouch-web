import { createContext } from "react";
import {
  PartnerDefinedInput,
  PouchLoyaltyRequestModel,
  PouchPayRequestModel,
} from "../../../temp/PartnerApiType";

export type IMyAccountContext = {
  partnerData: PartnerDefinedInput;
  updatePouchLoyaltyProgram: (value: PouchLoyaltyRequestModel) => void;
  updatePouchPayProgram: (value: PouchPayRequestModel) => void;
  logOut: () => void;
  setPartnerId: (partnerId: string) => void;
  partnerId: string | undefined;
  stripeId: string | undefined;
  externalAccounts: unknown;
};

export const MyAccountContext = createContext<IMyAccountContext | undefined>(
  undefined,
);
