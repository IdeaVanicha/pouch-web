"use client";
import { useContext, useEffect, useState } from "react";
import { SignUpContext } from "./SignUpContext";
import {
  CreatePartnerRequestModel,
  PartnerBusinessDetails,
} from "../../../temp/PartnerApiType";
import { httpsCallable } from "firebase/functions";
import { functions } from "@/firebase/firebase";

export type SignUpProviderProps = {
  children: React.ReactNode;
};

export const SignUpProvider: React.FC<SignUpProviderProps> = ({ children }) => {
  const [businessDetails, setBusinessDetails] =
    useState<PartnerBusinessDetails>({
      businessName: undefined,
      businessABN: undefined,
      businessEmail: undefined,
      businessType: undefined,
    });

  const updatesDetails = (input: Partial<PartnerBusinessDetails>) => {
    const newValue: PartnerBusinessDetails = {
      ...businessDetails,
      ...input,
    };
    setBusinessDetails(newValue);
  };

  const submit = async () => {
    const createPartner = httpsCallable(functions, "createPartnerAPI");
    const request: CreatePartnerRequestModel = {
      partner: {
        businessDetails: businessDetails,
        creditProgram: [],
        rewardProgram: [],
        bankDetails: "",
      },
    };
    await createPartner({ request }).then(async () => {});
  };

  useEffect(() => {
    const partners = httpsCallable(functions, "getPartnersWithUnverifiedABN");
    partners().then((res) => console.log(res));
  }, []);

  return (
    <SignUpContext.Provider value={{ businessDetails, updatesDetails, submit }}>
      {children}
    </SignUpContext.Provider>
  );
};

export const useSignUp = () => {
  const context = useContext(SignUpContext);
  if (context === undefined) {
    throw new Error("useSignUp must be used within a SignUpProvider");
  }
  return context;
};
