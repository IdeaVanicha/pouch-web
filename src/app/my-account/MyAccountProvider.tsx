"use client";
import { useContext, useEffect, useState } from "react";
import { MyAccountContext } from "./MyAccountContext";
import {
  Partner,
  PartnerDefinedInput,
  PouchLoyaltyRequestModel,
  PouchPayRequestModel,
} from "../../../temp/PartnerApiType";
import { auth, firestore, functions } from "@/firebase/firebase";
import { httpsCallable } from "firebase/functions";
import { useRouter } from "next/navigation";
import { DocumentData, doc, onSnapshot } from "firebase/firestore";
import { onAuthStateChanged, signOut } from "firebase/auth";

export type MyAccountProviderProps = {
  children: React.ReactNode;
};

export const MyAccountProvider: React.FC<MyAccountProviderProps> = ({
  children,
}) => {
  const router = useRouter();

  const [partnerId, setPartnerId] = useState<string | undefined>(undefined);
  const [partnerData, setPartnerData] = useState<PartnerDefinedInput>({
    businessDetails: {},
    creditProgram: [],
    rewardProgram: [],
    bankDetails: "bank-details",
  });

  const [stripeId, setStripeId] = useState<string | undefined>();
  const [externalAccounts, setExternalAccounts] = useState<
    Array<any> | undefined
  >();

  useEffect(() => {
    if (partnerId !== undefined) {
      getInitialData();
    }
  }, [partnerId]);

  const getInitialData = async () => {
    const getPartnerData = httpsCallable<
      { partnerId: string | undefined },
      Partner
    >(functions, "getPartnerById");

    const result = await getPartnerData({ partnerId: partnerId });

    const { data } = result || {};

    setPartnerData(data.partnerDefined);
    setStripeId(data.reference.stripeId);
    setExternalAccounts(data.reference.externalAccounts);

    if (externalAccounts === undefined || stripeId === undefined) {
      listenToStripeUpdate();
    } else {
      console.log("Adding external account");
    }
  };

  const updateData = (input: PartnerDefinedInput) => {
    const newValue: PartnerDefinedInput = {
      ...partnerData,
      ...input,
    };
    setPartnerData(newValue);
  };

  const listenToStripeUpdate = () => {
    if (partnerId !== undefined) {
      onSnapshot(doc(firestore, "Partners", partnerId), (data) => {
        const source = data.metadata.hasPendingWrites ? "Local" : "Server";
        console.log(source, " data: ", data.data());
        const { stripeId, externalAccounts } = data.data()?.reference;

        setStripeId(stripeId);
        setExternalAccounts(externalAccounts);
      });
    }
  };

  const updatePouchLoyaltyProgram = async (value: PouchLoyaltyRequestModel) => {
    const updateFunc = httpsCallable<
      { request: PouchLoyaltyRequestModel },
      Partner
    >(functions, "updatePouchLoyaltyProgram");

    if (partnerId === undefined) return;

    await updateFunc({ request: { ...value, partnerId: partnerId } }).then(
      (result) => {
        let rewardProgram = partnerData.rewardProgram;
        if (value.action === "ADD") {
          rewardProgram = [
            ...(partnerData.rewardProgram || []),
            { _id: result.data._id, partnerDefined: value.program },
          ];
        } else if (value.action === "DELETE") {
          rewardProgram = partnerData.rewardProgram?.filter(
            (program) => program._id !== value.programId,
          );
        }

        updateData({
          ...partnerData,
          rewardProgram: rewardProgram,
        });
      },
    );
  };

  const updatePouchPayProgram = async (value: PouchPayRequestModel) => {
    const updateFunc = httpsCallable<
      { request: PouchPayRequestModel },
      Partner
    >(functions, "updatePouchPayProgram");

    if (partnerId === undefined) return;

    await updateFunc({ request: { ...value, partnerId: partnerId } }).then(
      (result) => {
        if (result.data === undefined || result.data === null) return;

        let creditProgram = partnerData.creditProgram;

        if (value.action === "ADD") {
          creditProgram = [
            ...(partnerData.creditProgram || []),
            { _id: result.data._id, partnerDefined: value.program },
          ];
        } else if (value.action === "DELETE") {
          creditProgram = partnerData.creditProgram?.filter(
            (program) => program._id !== value.programId,
          );
        }
        updateData({
          ...partnerData,
          creditProgram: creditProgram,
        });
      },
    );
  };

  const logOut = () => {
    setPartnerData({
      businessDetails: {},
      creditProgram: [],
      rewardProgram: [],
      bankDetails: "bank-details",
    });
    signOut(auth);
    setPartnerId(undefined);
    router.push(`/my-account/login`);
  };

  return (
    <MyAccountContext.Provider
      value={{
        partnerData,
        updatePouchLoyaltyProgram,
        updatePouchPayProgram,
        logOut,
        setPartnerId,
        partnerId,
        stripeId,
        externalAccounts,
      }}
    >
      {children}
    </MyAccountContext.Provider>
  );
};

export const useMyAccount = () => {
  const context = useContext(MyAccountContext);
  if (context === undefined) {
    throw new Error("useMyAccount must be used within a MyAccountProvider");
  }
  return context;
};
