"use client";
import PillButton from "@/design-system/Button/PillButton";
import PillInputBar from "@/design-system/Input/PillInputBar";
import { Title } from "@/design-system/Text/Text";
import Image from "next/image";
import Link from "next/link";
import { MdEmail, MdLock } from "react-icons/md";
import logo from "./Screenshot 2024-03-18 at 11.22 1.png";
import { SignUpProvider, useSignUp } from "@/app/my-account/SignUpProvider";
import { BusinessType } from "../../../../temp/PartnerApiType";

export default function SignUp() {
  const { updatesDetails, submit } = useSignUp();

  return (
    <div className="h-full flex flex-col items-center justify-center">
      <div className="top-0 w-full absolute p-8">
        <Link href={"https://pouch-app.webflow.io/"} target="_blank">
          <Image src={logo} width={120} alt="Logo" />
        </Link>
      </div>
      <div className="w-96 h-96 flex flex-col items-center space-y-2">
        <Title value="Join Us 🤝️" classname="mb-6" />
        <PillInputBar
          onChange={() =>
            updatesDetails({ businessType: BusinessType.BeautySalon })
          }
          icon={<MdEmail color="#DDDDDD" />}
          placeholder="Business Type"
        />
        <PillInputBar
          onChange={(value) => updatesDetails({ businessName: value })}
          icon={<MdLock color="#DDDDDD" />}
          placeholder="Business Name"
        />
        <PillInputBar
          onChange={(value) => updatesDetails({ businessEmail: value })}
          icon={<MdLock color="#DDDDDD" />}
          placeholder="Business Email"
        />
        <PillInputBar
          onChange={(value) => updatesDetails({ businessABN: value })}
          icon={<MdLock color="#DDDDDD" />}
          placeholder="ABN"
        />

        <PillButton
          onClick={() => submit()}
          text="Submit"
          bgColor="#552084"
          variant="primary"
        />
        <div className="flex mt-6">
          <p>Already have an account? </p>
          <Link href={`/my-account/login`} className="text-yellow-400">
            Log In!
          </Link>
        </div>
      </div>
    </div>
  );
}
