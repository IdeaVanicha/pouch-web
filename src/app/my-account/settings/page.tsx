"use client";
import CardListItem from "@/design-system/Card/CardListItem";
import { MyAccountContainer } from "../MyAccountContainer";
import { MyAccountPage } from "../MyAccountPageTracker";
import CardDescription from "@/design-system/Card/CardDescription";
import PillButton from "@/design-system/Button/PillButton";
import AlertBox from "@/design-system/Alert/AlertBox";
import { useMyAccount } from "../MyAccountProvider";
import { functions } from "@/firebase/firebase";
import { httpsCallable } from "firebase/functions";

export default function Settings() {
  const { partnerData, partnerId, stripeId, externalAccounts } = useMyAccount();

  console.log(externalAccounts);
  const handleBilling = () => {
    const createConnectAccount = httpsCallable(
      functions,
      "createAndOnboardPartner",
    );
    createConnectAccount({
      request: {
        email: partnerData.businessDetails.businessEmail,
        partnerId: partnerId,
      },
    });
  };

  return (
    <MyAccountContainer
      currentPage={MyAccountPage.Settings}
      containerStyles="items-center"
    >
      <div className="w-full mt-20">
        <AlertBox variant="informative">
          <p>
            <span className="font-bold">Heads Up!</span> Your card is not live.
            To make it live fill in your bank details. Go to Settings {">"}{" "}
            Billing Details {">"} Send Form.
          </p>
        </AlertBox>
      </div>
      <div className="w-[750px] mt-2">
        <CardListItem
          header="ℹ️ General Information"
          description="Summary of all business details provided. To change our details make a request through pouch@gmail.com"
          showBottomBorder
          items={[
            {
              title: "Business Name",
              description: partnerData.businessDetails.businessName || "N/A",
            },
            {
              title: "Business ABN",
              description: partnerData.businessDetails.businessABN || "N/A",
            },
            {
              title: "Email",
              description: partnerData.businessDetails.businessEmail || "N/A",
            },
            {
              title: "Member Since",
              description: "12th December 2023",
            },
          ]}
        />
        {/* {stripeId && externalAccounts?.total_count > 0 ? (
          <CardListItem
            header="💰 Billing Details"
            description="Embark on a financial adventure with the key to your treasure trove! Behold, your dazzling bank details glitter below:"
            showBottomBorder
            items={[
              {
                title: "Bank Name",
                description: externalAccounts.data[0].bank_name || "N/A",
              },
              {
                title: "Last 4 Digits",
                description:
                  `**** **** ${externalAccounts.data[0].last4}` || "N/A",
              },
            ]}
          />
        ) : (
          <CardDescription
            header="Billing Details"
            description="To ensure high security, we've partnered with Stripe to collect your billing details. Fill out your billing details by clicking on the button below."
            showBottomBorder
          >
            <PillButton text="Send Form" onClick={() => handleBilling()} />
          </CardDescription>
        )} */}

        <CardDescription
          header="Staff Accounts"
          description=""
          showBottomBorder
        ></CardDescription>
      </div>
    </MyAccountContainer>
  );
}
