"use client";
import { usePathname, useRouter } from "next/navigation";
import "./myaccount.css";
import { MyAccountPage, myAccountPageConfig } from "./MyAccountPageTracker";
import CircularButton from "@/design-system/Button/CircularButton";
import PillButton from "@/design-system/Button/PillButton";
import Link from "next/link";
import classNames from "classnames";
import { TbSettingsFilled } from "react-icons/tb";
import { FaBell } from "react-icons/fa";
import { BodyText } from "@/design-system/Text/Text";
import { useMyAccount } from "./MyAccountProvider";

type MyAccountContainerProps = {
  currentPage: MyAccountPage;
  children: React.ReactNode;
  containerStyles?: string;
};

export const MyAccountContainer: React.FC<MyAccountContainerProps> = ({
  currentPage,
  children,
  containerStyles,
}: MyAccountContainerProps) => {
  const router = useRouter();
  return (
    <div className="w-full flex flex-col relative">
      <NavigationBar />
      <div
        className={classNames(
          "h-full w-full flex flex-col px-16 md:px-24 space-y-8 mt-16",
          containerStyles,
        )}
      >
        {children}
      </div>
    </div>
  );
};

const NavigationBar = () => {
  const pathname = usePathname();
  const { logOut } = useMyAccount();
  const router = useRouter();
  const getNavStyle = (url: string) => {
    return "/my-account/" + url === pathname
      ? "text-black font-bold"
      : "bg-none hover:text-black ";
  };

  return (
    <div className="fixed top-0 w-full items-center py-4 shadow flex border-grey p-2 px-6 bg-[#FFFFF]">
      <div className="flex items-center w-80">
        <p>
          <span className="font-bold">POUCH</span> @The Twelve Cafe
        </p>
      </div>
      <div className="flex w-full h-full items-center justify-center">
        {myAccountPageConfig.map((item, key) => (
          <div
            key={key}
            className={`mx-4 p-1 px-4 duration-100 rounded-lg ${getNavStyle(item.page)} active:scale-90`}
          >
            <Link href={item.page}>
              <BodyText value={item.displayName} />
            </Link>
          </div>
        ))}
      </div>
      <div className="flex items-center justify-center space-x-4">
        <CircularButton
          size="md"
          onClick={() => router.push("settings")}
          icon={<TbSettingsFilled color="#FFFFFF" size={20} />}
        />
        <CircularButton
          size="md"
          onClick={() => {}}
          icon={<FaBell color="#FFFFFF" />}
        />
        <div className="w-[200px]">
          <PillButton text="Log Out 👋" onClick={() => logOut()} />
        </div>
      </div>
    </div>
  );
};
