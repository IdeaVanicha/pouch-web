export enum MyAccountPage {
  PouchPay = "pouch-pay",
  PouchLoyalty = "pouch-loyalty",
  Settings = "settings",
  Dashboard = "dashboard",
}

type MyAccountPageConfigType = {
  page: MyAccountPage;
  displayName: string;
};

export const myAccountPageConfig: Array<MyAccountPageConfigType> = [
  {
    page: MyAccountPage.Dashboard,
    displayName: "Summary",
  },
  {
    page: MyAccountPage.PouchLoyalty,
    displayName: "Rewards",
  },
  {
    page: MyAccountPage.PouchPay,
    displayName: "Credit",
  },
];
