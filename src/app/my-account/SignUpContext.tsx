import { createContext } from "react";
import { PartnerBusinessDetails } from "../../../temp/PartnerApiType";

export type ISignUpContext = {
  businessDetails: PartnerBusinessDetails;
  updatesDetails: (input: PartnerBusinessDetails) => void;
  submit: () => void;
};

export const SignUpContext = createContext<ISignUpContext | undefined>(
  undefined,
);
