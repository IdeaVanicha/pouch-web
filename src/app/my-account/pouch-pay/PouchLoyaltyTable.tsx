import { FaTrashCan } from "react-icons/fa6";
import { Title } from "../../../design-system/Text/Text";
import { MdAdd } from "react-icons/md";

export type PouchPayTableType = {
  price: number;
  reward: number;
  icon: React.ReactElement;
  active: boolean;
};

type TableItemsType = {
  data: PouchPayTableType[];
  addItem: () => void;
};

export function PouchPayTable({ data, addItem }: TableItemsType) {
  if (data.length === 0) return;

  const PouchPayHeaders = ["Price (AUD)", "Reward", "Status", "Icon", "Remove"];

  return (
    <div className="w-full">
      <div className="flex justify-between my-4">
        <Title value="Credit Programs" classname="font-bold" />
        <button
          className="px-10 border-2 rounded-lg border-violet-900"
          onClick={addItem}
        >
          <MdAdd color="#552084" size={20} />
        </button>
      </div>
      <table className="w-full h-72 overscroll-scroll">
        <tr className="flex bg-[#FBF6FF] p-4 rounded-t-lg">
          {PouchPayHeaders.map((header, key) => (
            <th key={key} className="flex flex-1 justify-center font-normal">
              <p>{header}</p>
            </th>
          ))}
        </tr>
        {data.map((value, key) => (
          <tr
            className="w-full flex p-4 border-b-[0.5px] items-center"
            key={key}
          >
            <td className="w-full flex justify-center">$ {value.price}</td>
            <td className="w-full flex justify-center">$ {value.reward}</td>
            <td className="w-full flex justify-center">
              <div
                className={`w-20 border-2 h-8 rounded ${value.active && "bg-[#DCDCDC]"}`}
              >
                {value.active}
              </div>
            </td>
            <td className="w-full flex justify-center">{value.icon}</td>
            <td className="w-full flex justify-center">
              <button>
                <FaTrashCan color="#FFC04E" />
              </button>
            </td>
          </tr>
        ))}
      </table>
    </div>
  );
}
