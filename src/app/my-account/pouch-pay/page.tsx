"use client";
import CircularButton from "@/design-system/Button/CircularButton";
import { MyAccountContainer } from "../MyAccountContainer";
import { MyAccountPage } from "../MyAccountPageTracker";
import { FaPlus } from "react-icons/fa";
import { Title } from "@/design-system/Text/Text";
import PouchPayCard from "@/design-system/Miscellaneous/PouchPayCard";
import { AddPouchPayModal } from "@/design-system/Modal/AddPouchPayModal";
import { useState } from "react";
import { useMyAccount } from "../MyAccountProvider";
import { DeletePouchPayModal } from "@/design-system/Modal/DeletePouchPayModal";
import { PouchPayTableType, PouchPayTable } from "./PouchLoyaltyTable";

export default function PouchPayPage() {
  const [addPouchPayVisible, setAddPouchPayVisible] = useState(false);
  const [deleteItemModal, setDeleteItemModal] = useState<{
    visible: boolean;
    id: string | undefined;
  }>({ visible: false, id: undefined });
  const { partnerData, updatePouchPayProgram } = useMyAccount();
  const { creditProgram } = partnerData;
  const items: PouchPayTableType[] = [
    {
      price: 20,
      reward: 10,
      icon: <div className="w-10 h-10 border-2 bg-[#DCDCDC]" />,
      active: true,
    },
    {
      price: 20,
      reward: 10,
      icon: <div className="w-10 h-10 border-2 bg-[#DCDCDC]" />,
      active: true,
    },
    {
      price: 20,
      reward: 10,
      icon: <div className="w-10 h-10 border-2 bg-[#DCDCDC]" />,
      active: true,
    },
    {
      price: 20,
      reward: 10,
      icon: <div className="w-10 h-10 border-2 bg-[#DCDCDC]" />,
      active: true,
    },
    {
      price: 20,
      reward: 10,
      icon: <div className="w-10 h-10 border-2 bg-[#DCDCDC]" />,
      active: true,
    },
  ];
  return (
    <>
      <MyAccountContainer currentPage={MyAccountPage.PouchLoyalty}>
        <div className="w-full mt-16 flex justify-center items-center">
          <div className="w-[1200px]">
            <PouchPayTable
              data={items}
              addItem={() => setAddPouchPayVisible(true)}
            />
          </div>

          {/* <Title value="Active Credit Programs" />
          <CircularButton
            size="md"
            onClick={() => setAddPouchPayVisible(true)}
            icon={<FaPlus color="#ffffff" />}
          />
        </div>
        <div className="w-full grid lg:grid-cols-4 sm:grid-cols-2 grid-cols-1">
          {creditProgram &&
            creditProgram.map((value, key) => (
              <PouchPayCard
                program={value}
                key={key}
                deleteItem={() =>
                  setDeleteItemModal({ visible: true, id: value._id })
                }
              />
            ))} */}
        </div>
      </MyAccountContainer>
      <AddPouchPayModal
        close={() => setAddPouchPayVisible(false)}
        isVisible={addPouchPayVisible}
        updatePouchPayProgram={updatePouchPayProgram}
      />
      <DeletePouchPayModal
        close={() => setDeleteItemModal({ visible: false, id: undefined })}
        isVisible={deleteItemModal.visible}
        updatePouchPayProgram={updatePouchPayProgram}
        programId={deleteItemModal.id || ""}
      />
    </>
  );
}
