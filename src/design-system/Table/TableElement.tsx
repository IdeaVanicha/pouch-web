import { MdAdd } from "react-icons/md";
import { Title } from "../Text/Text";
import classNames from "classnames";

type TableTitleProps = {
  title: string;
  itemsCount: number;
  addItemButton: () => void;
};

export const TableTitle = ({
  title,
  itemsCount,
  addItemButton,
}: TableTitleProps) => {
  return (
    <div className="flex justify-between my-4">
      <Title value={`${title} (${itemsCount})`} classname="font-bold" />
      <button
        className="px-10 border-2 rounded-lg border-violet-900"
        onClick={addItemButton}
      >
        <MdAdd color="#552084" size={20} />
      </button>
    </div>
  );
};

type TableBodyProps<T extends Record<string, unknown>> = {
  headersElements: JSX.Element;
  dataElements: React.ReactElement;
};

export const TableBody = <T extends Record<string, unknown>>({
  headersElements,
  dataElements,
}: TableBodyProps<T>) => {
  return (
    <table className="w-full">
      <tr className="flex bg-[#FBF6FF] p-4 rounded-t-lg">{headersElements}</tr>
      <div className="w-full block h-72 overflow-scroll overscroll-x-hidden">
        {dataElements}
      </div>
    </table>
  );
};

export const TableRow = ({ children }: { children: React.ReactNode }) => {
  return (
    <tr className="w-full flex px-4 py-6 border-b-[0.5px] items-center">
      {children}
    </tr>
  );
};

export const TableColumn = ({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) => {
  return <td className={classNames("w-full flex", className)}>{children}</td>;
};

export const TableHeaderColumn = ({
  children,
  className,
}: {
  children: React.ReactNode;
  className?: string;
}) => {
  return (
    <th className={classNames("w-full flex font-normal", className)}>
      {children}
    </th>
  );
};
