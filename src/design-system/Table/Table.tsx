import { TableBody, TableTitle } from "./TableElement";

export type TableProps = {
  title: string;
  addItemButton: () => void;
  headersElements: JSX.Element;
  dataElements: JSX.Element;
  itemsCount: number;
};

export const Table: React.FC<TableProps> = ({
  title,
  addItemButton,
  headersElements,
  dataElements,
  itemsCount,
}: TableProps) => {
  return (
    <>
      <TableTitle
        title={title}
        addItemButton={addItemButton}
        itemsCount={itemsCount}
      />
      <TableBody
        headersElements={headersElements}
        dataElements={dataElements}
      />
    </>
  );
};
