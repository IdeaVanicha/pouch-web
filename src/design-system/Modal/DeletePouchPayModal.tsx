import PillButton from "@/design-system/Button/PillButton";
import { ModalComponentType } from "./ModalType";
import { PouchPayRequestModel } from "../../../temp/PartnerApiType";

type DeletePouchPayModalType = ModalComponentType & {
    updatePouchPayProgram: (value: PouchPayRequestModel) => void;
    programId: string;
};;

export const DeletePouchPayModal = ({ close, isVisible, updatePouchPayProgram, programId}: DeletePouchPayModalType) => {

    if (!isVisible) return;

    const deleteItem = async (value: PouchPayRequestModel) => {
        updatePouchPayProgram(value)
        close();
    }

    return <div className="fixed inset-0 bg-black-50/75 backdrop-blur-sm flex justify-center items-center align-center">
        <div className="w-[650px] border-2 rounded p-12 space-y-4 bg-white z-10 rounded-lg">
            <div className="w-full flex justify-between items-center">
                <h1 className="text-2xl">🗑️ Delete Item</h1>
            </div>
            <p>Are you sure you want to remove this item?</p>
            <div className="flex space-x-4">
            <PillButton text="Yes" variant="primary" onClick={() => deleteItem({
                partnerId: 'test-xxx',
                action: 'DELETE',
                programId: programId
            })}/>
            <PillButton text="No"  variant="secondary" onClick={close}/>
            </div>
        </div>
    </div>
}
