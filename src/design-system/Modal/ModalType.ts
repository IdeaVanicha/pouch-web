export type ModalComponentType = {
  isVisible: boolean;
  close: () => void;
};
