import PillButton from "@/design-system/Button/PillButton";
import { ModalComponentType } from "./ModalType";
import { PouchLoyaltyRequestModel } from "../../../temp/PartnerApiType";

type DeleteItemModalType = ModalComponentType & {
  updatePouchLoyaltyProgram: (value: PouchLoyaltyRequestModel) => void;
  programId: string;
};

export const DeleteItemModal = ({
  close,
  isVisible,
  updatePouchLoyaltyProgram,
  programId,
}: DeleteItemModalType) => {
  if (!isVisible) return;

  const deleteItem = async (value: PouchLoyaltyRequestModel) => {
    updatePouchLoyaltyProgram(value);
    close();
  };

  return (
    <div className="fixed inset-0 bg-black-50/75 backdrop-blur-sm flex justify-center items-center align-center">
      <div className="w-[650px] border-2 rounded p-12 space-y-4 bg-white z-10 rounded-lg">
        <div className="w-full flex justify-between items-center">
          <h1 className="text-2xl">🗑️ Delete Item</h1>
        </div>
        <p>Are you sure you want to remove this item?</p>
        <div className="flex space-x-4">
          <PillButton
            text="Yes"
            variant="primary"
            onClick={() =>
              deleteItem({
                partnerId: "",
                action: "DELETE",
                programId: programId,
              })
            }
          />
          <PillButton text="No" variant="secondary" onClick={close} />
        </div>
      </div>
    </div>
  );
};
