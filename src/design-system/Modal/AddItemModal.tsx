import CircularButton from "@/design-system/Button/CircularButton";
import PillButton from "@/design-system/Button/PillButton";
import PillInputBar from "@/design-system/Input/PillInputBar";
import { IoClose } from "react-icons/io5";
import { ModalComponentType } from "./ModalType";
import { FaStar, FaGift } from "react-icons/fa";
import {
  PartnerDefinedInput,
  PouchLoyaltyRequestModel,
} from "../../../temp/PartnerApiType";
import { functions } from "@/firebase/firebase";
import { httpsCallable } from "firebase/functions";
import { useState } from "react";
import { BodyText, Header, Subtitle, Title } from "../Text/Text";
import { MdAdd } from "react-icons/md";

type AddItemModalType = ModalComponentType & {
  updatePouchLoyaltyProgram: (input: PouchLoyaltyRequestModel) => void;
};

export const AddItemModal = ({
  close,
  isVisible,
  updatePouchLoyaltyProgram,
}: AddItemModalType) => {
  const [program, setProgram] = useState({ name: "", pointsRequired: 0 });

  const updateProgram = (value: string, type: "name" | "pointsRequired") => {
    setProgram({
      ...program,
      [type]: value,
    });
  };

  if (!isVisible) return;

  const submitItem = async (value: PouchLoyaltyRequestModel) => {
    updatePouchLoyaltyProgram(value);
    setProgram({ name: "", pointsRequired: 0 });
    close();
  };

  return (
    <div className="fixed inset-0 bg-black/40 backdrop-blur-sm backdrop-brightness-20 flex justify-center items-center align-center">
      <div className="w-[650px] px-14 py-10 space-y-8 bg-white z-10 rounded-[40px]">
        <div className="w-full flex flex-col space-y-2">
          <div className="flex justify-between items-center">
            <Title value="Add a reward" classname="font-semibold" />
            <button onClick={close}>
              <IoClose color="#552084" size={40} />
            </button>
          </div>
          <BodyText value="Enter the name of the reward, points required, and upload an image." />
        </div>
        <div className="flex space-x-8">
          <div className="flex flex-1 flex-col">
            <PillInputBar
              onChange={(value) => updateProgram(value, "name")}
              icon={<FaGift color="#000000" />}
              placeholder="Name of Item"
              className="border-[#DCDCDC] border-[1px]"
            />
            <PillInputBar
              onChange={(value) => updateProgram(value, "pointsRequired")}
              icon={<FaStar color="#000000" />}
              placeholder="Points Required"
              className="border-[#DCDCDC] border-[1px]"
            />
          </div>
          <div className="flex w-52 border-2 border-dashed rounded-[20px] justify-center items-center">
            <CircularButton
              onClick={() => {}}
              size="sm"
              icon={<MdAdd color="white" size={20} />}
            />
          </div>
        </div>

        <PillButton
          text="Submit"
          onClick={() =>
            submitItem({
              partnerId: "",
              action: "ADD",
              program: {
                name: program.name,
                pointsRequired: program.pointsRequired,
                reward: "20",
              },
            })
          }
        />
      </div>
    </div>
  );
};
