import CircularButton from "@/design-system/Button/CircularButton";
import PillButton from "@/design-system/Button/PillButton";
import PillInputBar from "@/design-system/Input/PillInputBar";
import { IoClose } from "react-icons/io5";
import { ModalComponentType } from "./ModalType";
import { FaGift, FaMoneyBillWave } from "react-icons/fa";
import { PouchPayRequestModel } from "../../../temp/PartnerApiType";
import { useState } from "react";

type AddPouchPayModalType = ModalComponentType & {
  updatePouchPayProgram: (value: PouchPayRequestModel) => void;
};

export const AddPouchPayModal = ({
  close,
  isVisible,
  updatePouchPayProgram,
}: AddPouchPayModalType) => {
  const [program, setProgram] = useState({ price: 0, creditAcquired: 0 });

  const updateProgram = (value: string, type: "price" | "creditAcquired") => {
    setProgram({
      ...program,
      [type]: value,
    });
  };

  if (!isVisible) return;

  const submitItem = async (value: PouchPayRequestModel) => {
    updatePouchPayProgram(value);
    setProgram({ price: 0, creditAcquired: 0 });
    close();
  };

  return (
    <div className="fixed inset-0 bg-black-50/75 backdrop-blur-sm flex justify-center items-center align-center">
      <div className="w-[650px] border-2 rounded p-12 space-y-4 bg-white z-10 rounded-lg border-black">
        <div className="w-full flex justify-between items-center">
          <h1 className="text-2xl">Add Program</h1>
          <CircularButton
            size="md"
            onClick={close}
            icon={<IoClose color="#ffffff" />}
          />
        </div>
        <PillInputBar
          onChange={(value) => updateProgram(value, "price")}
          icon={<FaMoneyBillWave color="#000000" />}
          placeholder="Required Spend"
        />
        <PillInputBar
          onChange={(value) => updateProgram(value, "creditAcquired")}
          icon={<FaGift color="#000000" />}
          placeholder="Extra Credit"
        />
        <PillButton
          text="Add"
          onClick={() =>
            submitItem({
              partnerId: "test-xxx",
              action: "ADD",
              program: {
                name: "test-1",
                price: program.price, //TODO: Change to decimal points
                creditAcquired: program.creditAcquired, //TODO: Change to decimal points
              },
            })
          }
        />
      </div>
    </div>
  );
};
