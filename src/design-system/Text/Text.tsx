
type TextPropsType = {
    value: string;
    classname?: string;
}

export const Title = ({value, classname}: TextPropsType) => <div className={`text-2xl ${classname} `}>{value}</div>
export const Header = ({value, classname}: TextPropsType) => <div className={`text-xl ${classname}`}>{value}</div>
export const Subtitle = ({value, classname}: TextPropsType) => <div className={`text-md ${classname}`}>{value}</div>
export const BodyText = ({value, classname}: TextPropsType) => <div className={`text-sm ${classname}`}>{value}</div>