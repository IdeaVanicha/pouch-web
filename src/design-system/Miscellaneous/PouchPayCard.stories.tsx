import type { Meta } from "@storybook/react";
import PouchPayCard from "./PouchPayCard";
import { PouchPay } from "../../../temp/PartnerApiType";

const program: PouchPay = {
  _id: "test-xxx",
  partnerDefined: {
    name: "name-xxx",
    price: 100,
    creditAcquired: 50,
  },
};

export const Variants = () => (
  <div className="w-full p-8 space-y-6">
    <p className="text-3xl mb-4">Card</p>
    <div className="border-[1px] w-[1200px] mb-8" />

    <div className="flex items-center space-evenly space-x-8">
      <p className="text-lg w-[150px]">PouchPayCard</p>
      <div className="w-[500px]">
        <PouchPayCard program={program} deleteItem={() => {}}></PouchPayCard>
      </div>
    </div>
  </div>
);

const meta = {
  title: "PouchPayCard",
  component: Variants,
  parameters: {
    layout: "fullscreen",
  },
} satisfies Meta<typeof PouchPayCard>;

export default meta;
