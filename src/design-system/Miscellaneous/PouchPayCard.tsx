import classNames from "classnames";
import { PouchPay, PouchPayRequestModel } from "../../../temp/PartnerApiType";
import { BodyText, Header } from "../Text/Text";
import CircularButton from "../Button/CircularButton";
import { FaTrashCan } from "react-icons/fa6";
import { SlGraph } from "react-icons/sl";

type PouchPayCardProps = {
    deleteItem: () => void;
    program: PouchPay;
} 

const PouchPayCard = ({deleteItem, program}: PouchPayCardProps) => {

    return <div className={
        classNames(
            "flex flex-col",
            "rounded",
            "border-2",
            "p-4",
            "justify-between",
            "border-black",
            "bg-white",
            "m-2",
            "flex-row",
            )
        }
    >
        <div className="h-8 flex justify-end">
            <p>Toggle here</p>
        </div>
        <div>
            <Header value={`Spend $${program.partnerDefined.price}`}/>
            <Header value={`For Extra $${program.partnerDefined.creditAcquired}`}/>
        </div>
        <div className="flex justify-between mt-4 flex-col lg:flex-row space-y-4">
            <div>
                <BodyText value="Created At"/>
                <BodyText value="12.03.2023"/>

            </div>
            
            <div className="flex space-x-2">
                <CircularButton onClick={() => {}} size="sm" icon={<SlGraph color="#ffffff"/>}/>
                <CircularButton onClick={deleteItem} size="sm" icon={<FaTrashCan color="#ffffff"/>}/>
            </div>
        </div>
    </div>
}

export default PouchPayCard;