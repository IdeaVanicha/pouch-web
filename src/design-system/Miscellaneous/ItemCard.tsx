import { FaTrashCan } from "react-icons/fa6"
import CircularButton from "../Button/CircularButton"
import { PouchLoyalty } from "../../../temp/PartnerApiType"
import { SlGraph } from "react-icons/sl"
import icecream from './iceCream.png'
import Image from "next/image"

export const ItemCard = ({ deleteItem, item }: { deleteItem: () => void, item: PouchLoyalty }) => {
    return <div className="w-72 space-y-4 items-center flex flex-col shrink-0 rounded">
        <div className="rounded flex bg-black flex-col p-2">
            <div className="relative border-2 border-black">
                <Image src={icecream} className="rounded w-full h-full" alt=""/>
            </div>
            <div className="bg-black text-white p-2">
                Name: {item.partnerDefined.name}{'\n'}
                <br/>
                Points Required: {item.partnerDefined.pointsRequired}
            </div>
        </div>
        <div className="flex space-x-4 py-2">
            <CircularButton onClick={() => {}} size="md" icon={<SlGraph color="#ffffff"/>}/>
            <CircularButton size="md" onClick={deleteItem} icon={<FaTrashCan color="#ffffff"/>}/>
        </div>
    </div>
}