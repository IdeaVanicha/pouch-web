import classNames from "classnames";

type CardDescriptionProps = {
    header: string;
    description: string;
    showBottomBorder?: boolean;
    children?: React.ReactNode;
    classnames?: string;
} 

const CardDescription = ({header, description, showBottomBorder, children, classnames}: CardDescriptionProps) => {

    return <div className={
        classNames(
            "flex",
            "space-y-8",
            "p-8", 
            "flex-col",
            showBottomBorder && 'border-b-2',
            classnames
            )
        }
    >
       <p className="text-2xl font-bold">{header}</p>
       <p className="text-base">{description}</p>
        {children}
    </div>
}

export default CardDescription;