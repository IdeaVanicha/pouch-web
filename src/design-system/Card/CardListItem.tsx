import classNames from "classnames";

type ListItemProp = {
    title: string;
    description: string;
}

type CardListItemProps = {
    header: string;
    description?: string;
    items: Array<ListItemProp>;
    showBottomBorder?: boolean;
    children?: React.ReactNode;
    classnames?: string;
    
} 

const CardListItem = ({header, description, items, showBottomBorder, children, classnames}: CardListItemProps) => {

    return <div className={
        classNames(
            "flex",
            "space-y-8",
            "p-8", 
            "flex-col",
            showBottomBorder && 'border-b-2',
            classnames
            )
        }
    >
       <p className="text-2xl font-bold">{header}</p>
       <p className="text-base">{description}</p>
       {
            items.map((item, key) => <div key={key} className="flex">
                <p className="font-bold w-48">{item.title}</p>
                <p>{item.description}</p>
            </div>
            )
       }
        {children}
    </div>
}

export default CardListItem;