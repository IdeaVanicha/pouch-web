import type { Meta } from "@storybook/react";
import CardDescription from "./CardDescription";
import CardListItem from "./CardListItem";
import PillButton from "../Button/PillButton";


export const Variants = () => (
  <div className="w-full p-8 space-y-6">
    <p className="text-3xl mb-4">Card</p>
    <div className="border-[1px] w-[1200px] mb-8"/>

    <div className="flex items-center space-evenly space-x-8">
      <p className="text-lg w-[150px]">CardDescription</p>
      <div className="w-[500px]">
        <CardDescription 
            header="General Information"
            description="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
        >
        </CardDescription>
      </div>
      <div className="w-[500px]">
        <CardDescription 
            header="General Information"
            description="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
            showBottomBorder
        >
        </CardDescription>
      </div>
    </div>

    <div className="flex items-center space-evenly space-x-8">
      <p className="text-lg w-[150px]">CardListItem</p>
      <div className="w-[500px]">
        <CardListItem 
            header="General Information"
            description="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
            items={[
              {
                title: 'Example 1',
                description: 'This is an example text for testing purposes.'
              },
              {
                title: 'Example 2',
                description: 'This is an example text for testing purposes.'
              },
            ]}
        >
        </CardListItem>
      </div>
      <div className="w-[500px]">
        <CardListItem 
            header="General Information"
            description="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
            showBottomBorder
            items={[
              {
                title: 'Example 1',
                description: 'This is an example text for testing purposes.'
              },
              {
                title: 'Example 2',
                description: 'This is an example text for testing purposes.'
              },
            ]}
        >
        </CardListItem>
      </div>
    </div>

    <div className="flex items-center space-evenly space-x-8">
      <p className="text-lg w-[150px]">CardListItem with Button</p>
      <div className="w-[500px]">
        <CardListItem 
            header="General Information"
            description="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s"
            items={[
              {
                title: 'Example 1',
                description: 'This is an example text for testing purposes.'
              },
              {
                title: 'Example 2',
                description: 'This is an example text for testing purposes.'
              },
            ]}
        >
          <PillButton text="Submit" onClick={() => {}}/>
        </CardListItem>
      </div>
    </div>

  </div>
)

const meta = {
  title: "Card",
  component: Variants,
  parameters: {
    layout: "fullscreen",
  },
} satisfies Meta<typeof CardDescription>;

export default meta;

