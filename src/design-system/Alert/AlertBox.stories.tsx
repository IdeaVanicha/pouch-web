import type { Meta } from "@storybook/react";
import AlertBox from "./AlertBox";


export const Variants = () => (
  <div className="w-full p-8 space-y-6">
    <p className="text-3xl mb-4">Alertbox</p>
    <div className="border-[1px] w-[1200px] mb-8"/>

    <div className="flex flex-col space-y-4">
      <p className="text-lg w-[80px]">Informative</p>
      <div>
        <AlertBox variant="informative">
            <p><span className="font-bold">Heads Up!</span> Your card is not live. To make it live fill in your bank details. Go to Settings {">"} Billing Details {">"} Send Form.</p>
        </AlertBox>
      </div>
    </div>

    <div className="flex flex-col space-y-4">
      <p className="text-lg w-[80px]">Error</p>
      <div>
        <AlertBox variant="error">
            <p><span className="font-bold">Error:</span> Your bank details is incorrect and needs updating now.</p>
        </AlertBox>
      </div>
    </div>
  </div>
)

const meta = {
  title: "Alert",
  component: Variants,
  parameters: {
    layout: "fullscreen",
  },
} satisfies Meta<typeof AlertBox>;

export default meta;

