import type { Meta, StoryObj } from "@storybook/react";
import PillInputBar from "./PillInputBar";
import { FaSearch } from "react-icons/fa";

export const Variants = () => (
  <div className="w-full p-8 space-y-6">
    <p className="text-3xl mb-4">Pill Input</p>
    <div className="border-[1px] w-[1200px] mb-8"/>
    <div className="flex items-center space-x-8">
      <p className="text-lg w-[80px]">Primary</p>
      <div className="w-[300px]">
        <PillInputBar icon={<FaSearch color="#eeeeee"/>} onChange={() => {}} placeholder="Insert an input"/>
      </div>
    </div>
    <div className="flex items-center space-x-8">
      <p className="text-lg w-[80px]">Error</p>
      <div className="w-[300px]">
        <PillInputBar icon={<FaSearch color="#eeeeee"/>}  onChange={() => {}} placeholder="Insert an input" variant="error" errorText="Insert an error text"/>
      </div>
    </div>
  </div>
)

const meta = {
  title: "InputBar",
  component: Variants,
  tags: ["autodocs"],
  parameters: {
    layout: "fullscreen",
  },
} satisfies Meta<typeof PillInputBar>;

export default meta;

