import classNames from "classnames";

type CircularButtonProps = {
  size: "lg" | "md" | "sm";
  onClick: () => void;
  icon?: React.ReactElement;
};

const CircularButton = ({
  size = "md",
  onClick,
  icon,
}: CircularButtonProps) => {
  const sizeConfig = {
    lg: "w-16 h-16",
    md: "w-12 h-12",
    sm: "w-8 h-8",
  };

  return (
    <button
      onClick={onClick}
      className={classNames(
        "flex",
        "items-center",
        "justify-center",
        "rounded-full",
        "bg-amber-500",
        "hover:scale-105 active:scale-95 hover:cursor-pointer",
        sizeConfig[size],
      )}
    >
      {icon}
    </button>
  );
};

export default CircularButton;
