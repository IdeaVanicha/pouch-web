import classNames from "classnames";
import "../global.css";

type PillButtonProps = {
  text: string;
  disabled?: boolean;
  variant?: "primary" | "secondary";
  onClick: () => void;
  bgColor?: string;
  className?: string;
};

const PillButton = ({
  text,
  disabled = false,
  variant = "primary",
  onClick,
  bgColor = "#000000",
  className,
}: PillButtonProps) => {
  const stylingConfig = {
    primary: {
      default: `bg-violet-950 text-white text-white`,
      disabled: "hover:cursor-not-allowed !bg-[#eeeeee] text-black border-none",
      text: `text-white`,
      disabledText: `text-black`,
    },
    secondary: {
      default: `bg-transparent border-[${bgColor}] border-2`,
      disabled:
        "hover:cursor-not-allowed border-slate-400 border-2 text-black border-none",
      text: `text-black font-semibold`,
      disabledText: `text-black`,
    },
  };

  return (
    <button
      disabled={disabled}
      onClick={onClick}
      className={classNames(
        "flex",
        "items-center",
        "justify-center",
        "rounded-full",
        "w-full",
        "py-4",
        stylingConfig[variant].default,
        "disabled:opacity-40 disabled:cursor-not-allowed disabled:text-black",
        "hover:scale-105 active:scale-95 hover:cursor-pointer",
        className,
      )}
    >
      <p className={classNames(stylingConfig[variant].text, "text-base")}>
        {text}
      </p>
    </button>
  );
};

export default PillButton;
