import type { Meta, StoryObj } from "@storybook/react";
import PillButton from "./PillButton";
import CircularButton from "./CircularButton";

export const Variants = () => (
  <div className="w-full p-8 space-y-6">
    <p className="text-3xl mb-4">Pill Buttons</p>
    <div className="border-[1px] w-[1200px] mb-8"/>
    <div className="flex items-center space-x-8">
      <p className="text-lg w-[80px]">Primary</p>
      <div className="w-[300px]">
        <PillButton text="Button text" onClick={() => {}}/>
      </div>
      <div className="w-[300px]">
        <PillButton text="Button text" disabled onClick={() => {}}/>
      </div>
    </div>

    <div className="flex items-center space-x-8">
      <p className="text-lg w-[80px]">Secondary</p>
      <div className="w-[300px]">
        <PillButton text="Button text" variant="secondary" onClick={() => {}}/>
      </div>
      <div className="w-[300px]">
        <PillButton text="Button text" disabled variant="secondary" onClick={() => {}}/>
      </div>
    </div>

    <div>
      <p className="text-3xl mb-4">Circular Buttons</p>
      <div className="border-[1px] w-[1200px] mb-8"/>
      <div className="flex items-center space-x-8">
        <CircularButton onClick={() => {}} size="lg"/>
        <CircularButton onClick={() => {}} size="md"/>
        <CircularButton onClick={() => {}} size="sm"/>

      </div>
    </div>


  </div>
)

const meta = {
  title: "Buttons",
  component: Variants,
  tags: ["autodocs"],
  parameters: {
    layout: "fullscreen",
  },
} satisfies Meta<typeof PillButton>;

export default meta;

