import classNames from "classnames";
import '../global.css'
import { BodyText } from "../Text/Text";

type PillButtonProps = {
    text: string;
    disabled?: boolean;
    variant?: 'primary' | 'secondary';
    onClick: () => void;
} 

const PillRadioButton = ({text, disabled = false, variant = 'primary', onClick}: PillButtonProps) => {

    return <div>
        <input type="radio"/>
        <BodyText value={text}/>
    </div>
}

export default PillRadioButton;