import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getFunctions } from "firebase/functions";

const environment = {
  production: false,
  apiKey: "AIzaSyD_VmMwHJ6WZYUP8UGX-ByCQuJbZ7Ob-S8",
  authDomain: "pouch-development.firebaseapp.com",
  projectId: "pouch-development",
  storageBucket: "pouch-development.appspot.com",
  messagingSenderId: "389607564579",
  appId: "1:389607564579:web:f0e4914acc3a058a4b9896",
  measurementId: "G-K301PFG64H",
};

const app = initializeApp(environment);
const firestore = getFirestore(app);
const auth = getAuth(app);
const functions = getFunctions(app, "australia-southeast1");

export { app, firestore, functions, auth };
