export type CreatePartnerRequestModel = {
  partner: PartnerDefinedInput;
  prespecifiedPartnerId?: string;
};

export type GetPartnerRequestModel = {
  partnerId: string;
};

export type PatchPartnerBusinessDetailsRequestModel = {
  partnerId: string;
  businessDetails: PartnerBusinessDetails;
};

export type PartnerDefinedInput = {
  businessDetails: PartnerBusinessDetails;
  creditProgram?: Array<PouchPay>;
  rewardProgram?: Array<PouchLoyalty>;
  bankDetails?: string;
};

export type PartnerReference = {
  status: PartnerStatus;
  createdAt: Date;
  stripeId?: string;
  externalAccounts: any;
};

export enum PartnerStatus {
  VERIFIED = "VERIFIED",
  UNVERIFIED = "UNVERIFIED",
  // Add more status later
}

export type PartnerBusinessDetails = {
  businessName?: string;
  businessABN?: string;
  businessEmail?: string;
  businessType?: BusinessType;
};

export enum BusinessType {
  Restaurant = "Restaurant",
  BeautySalon = "BeautySalon",
  Supermarket = "Supermarket",
  Other = "Other",
}

export type PouchPay = {
  _id: string;
  partnerDefined: PouchPayPartnerDefined;
};

export type PouchPayPartnerDefined = {
  name: string;
  price: number; //TODO: Change to decimal points
  creditAcquired: number; //TODO: Change to decimal points
};

export type PouchLoyalty = {
  _id: string;
  partnerDefined: PouchLoyaltyPartnerDefined;
};

export type PouchLoyaltyPartnerDefined = {
  name: string;
  pointsRequired: number;
  reward: string;
};

export type PouchLoyaltyRequestModel = {
  partnerId: string;
} & (
  | { action: "ADD"; program: PouchLoyaltyPartnerDefined }
  | { action: "DELETE"; programId: string }
);

export type PouchPayRequestModel = {
  partnerId: string;
} & (
  | { action: "ADD"; program: PouchPayPartnerDefined }
  | { action: "DELETE"; programId: string }
);

export type Partner = {
  _id: string;
  reference: PartnerReference;
  partnerDefined: PartnerDefinedInput;
};
